# Travel Note

## Description

This app is for anyone who wants to keep their travel records. Developing your world travel map, add countries where you've traveled, cities with photos and notes.

## DEMO

![](https://firebasestorage.googleapis.com/v0/b/travel-note-84771.appspot.com/o/readme%2Ftravel_note_screenrecording.gif?alt=media&token=7f009168-7209-4f91-86eb-dc4c312961e6)

## Requirement

"bcryptjs": "^2.4.3",
"config": "^3.3.1",
"connect-redis": "^4.0.4",
"cookie-parser": "^1.4.5",
"express": "^4.17.1",
"express-session": "^1.17.1",
"express-validator": "^6.4.0",
"firebase": "^7.15.0",
"gravatar": "^1.8.0",
"jsonwebtoken": "^8.5.1",
"moment": "^2.26.0",
"mongoose": "^5.9.6",
"react": "^16.13.1",
"react-dom": "^16.13.1",
"react-scripts": "3.4.1",
"redis": "^3.0.2",
"request": "^2.88.2"

## Available Scripts

### `npm run dev`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />

### `cd client && npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.
