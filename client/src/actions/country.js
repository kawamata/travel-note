import axios from 'axios';
import {
  GET_COUNTRY,
  COUNTRY_ERROR,
  ADD_CITY,
  CITY_ERROR,
  EMPTY_CITY,
  DELETE_COUNTRY,
  COUNTRY_DELETE_ERROR,
  GET_ALLCOUNTRIES,
  COUNTRIES_ERROR,
} from './types';
import { setAlert } from './alert';

// Get Country
export const getCountry = (country_id) => async (dispatch) => {
  try {
    const res = await axios.get(`/api/country/${country_id}`);
    dispatch({
      type: GET_COUNTRY,
      payload: res.data,
    });
    // CityからCountryページに戻った時に、stateのcityを空にする
    dispatch({ type: EMPTY_CITY });
  } catch (err) {
    dispatch({
      type: COUNTRY_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};

// Post City to Country
export const addCity = (formData, country_id) => async (dispatch) => {
  try {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const res = await axios.post(`/api/cities/${country_id}`, formData, config);
    dispatch({
      type: ADD_CITY,
      payload: res.data,
    });
    dispatch(setAlert('City Added', 'success'));
  } catch (err) {
    dispatch({
      type: CITY_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};

// Delete Country
export const deleteCountry = (country_id) => async (dispatch) => {
  try {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const res = await axios.delete(
      '/api/country',
      { params: { country_id: country_id } },
      config
    );
    dispatch({ type: DELETE_COUNTRY, payload: res.data });
    dispatch(setAlert('Country Deleted', 'success'));
  } catch (err) {
    dispatch({
      type: COUNTRY_DELETE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};

// Get Countries
export const getAllCountries = () => async (dispatch) => {
  try {
    const res = await axios.get('/api/countries/my');
    dispatch({
      type: GET_ALLCOUNTRIES,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: COUNTRIES_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};
