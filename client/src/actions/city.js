import axios from 'axios';
import { setAlert } from './alert';

import {
  GET_CITY,
  GET_CITY_ERROR,
  ADD_CITYNOTE,
  CITYNOTE_ERROR,
  DELETE_PHOTO,
  DELETE_PHOTO_ERROR,
  DELETE_NOTE,
  DELETE_NOTE_ERROR,
  DELETE_CITY,
  DELETE_CITY_ERROR,
  EDIT_NOTE,
  EDIT_NOTE_ERROR,
  GET_THISCOUNTRY,
  THISCOUNTRY_ERROR,
} from './types';

// Get City
export const getCity = (country_id, city_id) => async (dispatch) => {
  try {
    const res = await axios.get(`/api/city/${country_id}/${city_id}`);
    dispatch({
      type: GET_CITY,
      payload: res.data[0],
    });
  } catch (err) {
    dispatch({
      type: GET_CITY_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};

// Post City - note
export const addCityNote = (country_id, city_id, note) => async (dispatch) => {
  try {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const res = await axios.post(
      `/api/city/${country_id}/${city_id}/note`,
      { note: note },
      config
    );

    dispatch({
      type: ADD_CITYNOTE,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: CITYNOTE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};

// Delete City - photo
export const deletePhoto = (country_id, city_id, photo_id) => async (
  dispatch
) => {
  try {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const data = { country_id: country_id, city_id: city_id };
    const res = await axios.delete(
      `/api/city/photo/${photo_id}`,
      { params: data },
      config
    );
    dispatch({
      type: DELETE_PHOTO,
      payload: res.data,
    });
    dispatch(setAlert('Photo Deleted', 'success'));
  } catch (err) {
    dispatch({
      type: DELETE_PHOTO_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};

// Delete City - note
export const deleteNote = (country_id, city_id, note_id) => async (
  dispatch
) => {
  try {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const data = { country_id: country_id, city_id: city_id };
    const res = await axios.delete(
      `/api/city/note/${note_id}`,
      { params: data },
      config
    );
    dispatch({
      type: DELETE_NOTE,
      payload: res.data,
    });
    dispatch(setAlert('Note Deleted', 'success'));
  } catch (err) {
    dispatch({
      type: DELETE_NOTE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};

// Delete City
export const deleteCity = (country_id, city_id) => async (dispatch) => {
  try {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const data = { country_id: country_id, city_id: city_id };
    const res = await axios.delete('/api/city', { params: data }, config);
    dispatch({
      type: DELETE_CITY,
      payload: res.data,
    });
    dispatch(setAlert('City Deleted', 'success'));
  } catch (err) {
    dispatch({
      type: DELETE_CITY_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};

// Put City - note
export const editNote = (country_id, city_id, note, note_id) => async (
  dispatch
) => {
  try {
    const config = {
      headers: { 'Content-Type': 'application/json' },
    };
    // const body = JSON.stringify({ country_id, city_id, note });
    const body = { country_id: country_id, city_id: city_id, note: note };
    // console.log('=======', note_id);
    const res = await axios.put(`/api/city/note/${note_id}`, body, config);
    dispatch({
      type: EDIT_NOTE,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: EDIT_NOTE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};

// Get Country
export const getThisCountry = (country_id) => async (dispatch) => {
  try {
    const res = await axios.get(`/api/country/${country_id}`);
    dispatch({
      type: GET_THISCOUNTRY,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: THISCOUNTRY_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};
