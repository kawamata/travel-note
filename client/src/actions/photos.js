import axios from 'axios';
import firebase from '../firebase/firebase';

import {
  ADD_PHOTOURL_COUNTRY,
  ADD_PHOTOURL_CITY,
  PHOTOURL_ERROR,
} from './types';

// Post photos to Firebase storage
export const postPhotos = (
  files,
  user,
  country_id,
  city_id,
  comment,
  isCity
) => async (dispatch) => {
  files.forEach((file) => {
    const ref = firebase.storage().ref(`/image/${user}/${file.path}`);
    const metadata = { contentType: file.type };
    const task = ref.child(file.path).put(file, metadata);
    task
      .then((snapshot) => snapshot.ref.getDownloadURL())
      .then((url) => {
        if (isCity) {
          dispatch(postPhotoURLCity(url, country_id, city_id, comment));
        } else {
          dispatch(postPhotoURLCountry(url, country_id, city_id, comment));
        }
      })
      .catch((error) => {
        console.log('error', error);
      });
  });
};

// Post photo URL for Country (output country)
export const postPhotoURLCountry = (
  url,
  country_id,
  city_id,
  comment
) => async (dispatch) => {
  try {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const body = { photo: url, note: comment };
    const res = await axios.post(
      `/api/cities/${country_id}/${city_id}/photos`,
      body,
      config
    );

    dispatch({
      type: ADD_PHOTOURL_COUNTRY,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: PHOTOURL_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};

// Post photo URL for City (output city)
export const postPhotoURLCity = (url, country_id, city_id, comment) => async (
  dispatch
) => {
  try {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const body = { photo: url, note: comment };
    const res = await axios.post(
      `/api/cities/${country_id}/${city_id}/photos`,
      body,
      config
    );

    const city = res.data.cities.filter((city) => city._id === city_id);
    dispatch({
      type: ADD_PHOTOURL_CITY,
      payload: city[0],
    });
  } catch (err) {
    dispatch({
      type: PHOTOURL_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};
