import uuidv4 from 'uuid/v4';

import { SET_ALERT, REMOVE_ALERT } from './types';

export const setAlert = (msg, alertType) => (dispatch) => {
  // uuidで一意のidを生成（alert領域でのみ使用）
  const id = uuidv4();
  dispatch({
    type: SET_ALERT,
    payload: { msg, alertType, id },
  });

  // dispatchを使わないと送信できなかった
  setTimeout(() => dispatch({ type: REMOVE_ALERT, payload: id }), 5000);
};

export function closeAlert(id) {
  return {
    type: REMOVE_ALERT,
    payload: id,
  };
}
// この書き方もOK
// export const closeAlert = (id) => (dispatch) => {
//   dispatch({
//     type: REMOVE_ALERT,
//     payload: id,
//   });
// };
