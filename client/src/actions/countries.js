import axios from 'axios';
import { setAlert } from './alert';
import {
  GET_MYCOUNTRIES,
  COUNTRIES_ERROR,
  ADD_COUNTRY,
  COUNTRY_ERROR,
  GET_CITIES,
  CITIES_ERROR,
  CLEAR_USER_COUNTRIES,
} from './types';

// Get Countries
export const getCountries = () => async (dispatch) => {
  try {
    const res = await axios.get('api/countries/my');
    dispatch({
      type: GET_MYCOUNTRIES,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: COUNTRIES_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};

// Post Countries
export const addCountries = (formData) => async (dispatch) => {
  console.log('formData', formData);
  try {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const res = await axios.post('api/countries', formData, config);
    dispatch({
      type: ADD_COUNTRY,
      payload: res.data,
    });
    dispatch(setAlert('Country Added', 'success'));
  } catch (err) {
    dispatch({
      type: COUNTRY_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};

// Get Cities
export const getCities = (country_id) => async (dispatch) => {
  try {
    const res = await axios.get(`/api/cities/${country_id}`);
    dispatch({
      type: GET_CITIES,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: CITIES_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};

// Clear Countries
export const clearCountries = () => async (dispatch) => {
  dispatch({ type: CLEAR_USER_COUNTRIES });
};
