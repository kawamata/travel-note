import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { login } from '../../actions/auth';

import Button from '../parts/Button';

import './Auth.scss';

const Login = ({ login, isAuthenticated }) => {
  const [formData, setFormData] = useState({
    email: '',
    password: '',
  });
  const { email, password } = formData;

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = (e) => {
    e.preventDefault();
    login(email, password);
  };

  if (isAuthenticated) {
    return <Redirect to='/top' />;
  }
  return (
    <div className='login auth'>
      <div className='container'>
        <div className='inner'>
          <h1>Login to Your Account</h1>
          <form className='forms' onSubmit={(e) => onSubmit(e)}>
            <div className='form-group'>
              <input
                className='input-field'
                type='text'
                placeholder='Email'
                name='email'
                value={email}
                onChange={(e) => onChange(e)}
              />
              <input
                className='input-field'
                type='password'
                placeholder='Password'
                name='password'
                value={password}
                onChange={(e) => onChange(e)}
              />
            </div>
            <Button size='m' color='green' text='Login' />
          </form>
          <p className='note'>
            Don't have an account? &nbsp;<Link to='/register'>Sign Up</Link>
          </p>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

Login.propTypes = {
  login: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool,
};

export default connect(mapStateToProps, { login })(Login);
