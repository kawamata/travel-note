import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { setAlert } from '../../actions/alert';
import { register } from '../../actions/auth';

import Button from '../parts/Button';

import './Auth.scss';

const Register = ({ setAlert, register, isAuthenticated }) => {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    password: '',
    password2: '',
  });
  const { name, email, password, password2 } = formData;

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = (e) => {
    e.preventDefault();
    if (password !== password2) {
      setAlert('Password do not match', 'danger');
    } else {
      register({ name, email, password });
    }
  };

  if (isAuthenticated) {
    return <Redirect to='/top' />;
  }

  return (
    <div className='register auth'>
      <div className='container'>
        <div className='inner'>
          <h1>Create Your Account</h1>
          <form className='forms' onSubmit={(e) => onSubmit(e)}>
            <div className='form-group'>
              <input
                className='input-field'
                type='text'
                placeholder='Name'
                name='name'
                value={name}
                onChange={(e) => onChange(e)}
              />
              <input
                className='input-field'
                type='text'
                placeholder='Email Adress'
                name='email'
                value={email}
                onChange={(e) => onChange(e)}
              />
              <small>
                *This site uses Gravatar so if you want a profile image, please
                use a Gravatar email
              </small>
              <hr />
              <input
                className='input-field'
                type='password'
                placeholder='Password'
                name='password'
                value={password}
                onChange={(e) => onChange(e)}
              />
              <input
                className='input-field'
                type='password'
                placeholder='Confirm Password'
                name='password2'
                value={password2}
                onChange={(e) => onChange(e)}
              />
            </div>
            <Button size='m' color='orange' text='Register' />
          </form>
          <p className='note'>
            Already have an account? &nbsp;<Link to='/login'>Log In</Link>
          </p>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

Register.propTypes = {
  setAlert: PropTypes.func.isRequired,
  register: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool,
};

export default connect(mapStateToProps, { setAlert, register })(Register);
