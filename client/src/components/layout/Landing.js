import React from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from '../parts/Button';

import './Landing.scss';

const Landing = ({ isAuthenticated }) => {
  if (isAuthenticated) {
    return <Redirect to='/top' />;
  }
  return (
    <section className='landing'>
      <div className='container'>
        <div className='inner'>
          <p className='welcome'>
            <span>Welcome to Travel Note</span>
            <br />
            Create your travel record with photos and notes and share it with
            people.
          </p>
          <div className='buttons'>
            <Button color='brown' size='m' link='/register' text='Sign Up' />
            <hr />
            <Button color='blue' size='m' link='/login' text='Log In' />
          </div>
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

Landing.propTypes = {
  isAuthenticated: PropTypes.bool,
};

export default connect(mapStateToProps, null)(Landing);
