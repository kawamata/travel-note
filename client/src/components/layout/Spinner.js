import React, { Fragment } from 'react';
import spinner from '../../img/spin.gif';
import './Spinner.scss';

export default () => {
  return (
    <Fragment>
      <img className='spinner' src={spinner} alt='Loading..' />
    </Fragment>
  );
};
