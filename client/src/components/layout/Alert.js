import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { closeAlert } from '../../actions/alert';

import './Alert.scss';

const Alert = ({ alerts, closeAlert }) => {
  const onClick = (e, id) => {
    closeAlert(id);
  };
  return (
    alerts !== null &&
    alerts.length > 0 &&
    alerts.map((alert) => (
      <div key={alert.id} className={`alert banner-${alert.alertType}`}>
        {alert.msg}
        <span className='close' onClick={(e) => onClick(e, alert.id)}>
          <i className='fas fa-star'></i>
        </span>
      </div>
    ))
  );
};

Alert.propTypes = {
  closeAlert: PropTypes.func.isRequired,
  alerts: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  alerts: state.alert,
});

export default connect(mapStateToProps, { closeAlert })(Alert);
