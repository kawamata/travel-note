import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { logout } from '../../actions/auth';

import './Navbar.scss';

export const Navbar = ({ auth: { isAuthenticated, loading }, logout }) => {
  // スマホメニュー用のiconトグル
  const [open, setOpen] = useState();
  const toggle = () => setOpen(!open);
  const logoutUser = () => {
    toggle();
    logout();
  };
  // const isClose = () => setOpen(false);

  const authLinks = (
    <ul className={classNames('links', `${open ? 'isOpen' : ''}`)}>
      <li>
        <Link to='/' onClick={toggle}>
          Top
        </Link>
      </li>
      {/* <li>
        <a>Add Country</a>
      </li> */}
      <li>
        <a onClick={logoutUser}>Logout</a>
      </li>
    </ul>
  );
  const guestLinks = (
    <ul className={classNames('links', `${open ? 'isOpen' : ''}`)}>
      <li>
        <Link to='/' onClick={toggle}>
          Top
        </Link>
      </li>
      <li>
        <Link to='/register' onClick={toggle}>
          Register
        </Link>
      </li>
      <li>
        <Link to='/login' onClick={toggle}>
          Login
        </Link>
      </li>
    </ul>
  );
  return (
    <nav>
      <div className='navbar'>
        <h1>
          <Link to='/' className='title'>
            Travel Note
          </Link>
        </h1>
        {!loading && isAuthenticated ? authLinks : guestLinks}

        {/* smartphone */}
        <i
          className={classNames(
            'fas fa-align-justify fa-2x smartphone ham',
            `${open ? 'isOpen' : ''}`
          )}
          onClick={toggle}
        ></i>
        <i
          className={classNames(
            'fas fa-star fa-2x smartphone star',
            `${open ? 'isOpen' : ''}`
          )}
          onClick={toggle}
        ></i>
      </div>
      <div className={classNames('menu', `${open ? 'isOpen' : ''}`)}></div>
    </nav>
  );
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

Navbar.propTypes = {
  auth: PropTypes.object.isRequired,
  login: PropTypes.func,
};

export default connect(mapStateToProps, { logout })(Navbar);
