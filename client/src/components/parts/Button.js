import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router-dom';

import './Button.scss';

export const colors = ['brown', 'blue', 'green', 'orange', 'gray', 'red-frame'];
export const sizes = ['xs', 's', 'm', 'l'];

const Button = ({ text, color, size, link }) => {
  const withLink = (
    <Link
      to={link}
      className={classNames('btn', `color-${color}`, `size-${size}`)}
    >
      {text}
    </Link>
  );
  const submit = (
    <input
      type='submit'
      className={classNames('btn', `color-${color}`, `size-${size}`)}
      value={text}
    />
  );
  return <Fragment>{link ? withLink : submit}</Fragment>;
};

Button.propTypes = {
  text: PropTypes.string.isRequired,
  color: PropTypes.oneOf(colors),
  size: PropTypes.oneOf(sizes),
  link: PropTypes.string.isRequired,
};

Button.defaultProps = {
  text: '',
  color: '',
  size: 'm',
  link: '',
};

export default Button;
