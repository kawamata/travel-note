import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import './Modal.scss';

import Button from '../parts/Button';

const Modal = ({ onClose, name, deleteAction }) => {
  const onDelete = () => {
    // モーダルopen時の背景固定を解除
    document.body.setAttribute('style', 'overflow: none');
    deleteAction();
  };
  return (
    <div className='modal-background'>
      <div className='modal-frame'>
        <i className='fas fa-times' onClick={() => onClose(false)}></i>
        <div className='text'>
          <p>
            Are you sure to delete <span>{name}</span> ?
          </p>
          <small>
            All data including photos and notes will be deleted too.
          </small>
          <hr />
          <div className='btn-wrapper' onClick={onDelete}>
            <Button color='blue' size='s' text='Yes' />
          </div>
          <div className='btn-wrapper' onClick={() => onClose(false)}>
            <Button color='gray' size='s' text='No' />
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(connect(null, null)(Modal));
