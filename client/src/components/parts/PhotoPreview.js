import React, { useState } from 'react';
import { connect } from 'react-redux';

import './PhotoPreview.scss';

const PhotoPreview = ({
  setOpen,
  photoData: { photoUrl, photoComment },
  photos,
}) => {
  // デフォルトのurlとcomment
  const [showPhotoData, setShowPhotoData] = useState({
    url: photoUrl,
    comment: photoComment,
  });
  const { url, comment } = showPhotoData;

  const nextPhoto = (nextIndex) => {
    setShowPhotoData({
      url: photos[nextIndex].photo,
      comment: photos[nextIndex].note,
    });
  };

  const checkURL = () => {
    const index = photos.findIndex(({ photo }) => photo === url);
    const indexes = photos.length;
    const items = [];
    if (indexes > index + 1) {
      items.push(
        <div className='right-arrow' key='1'>
          <i
            className='fas fa-caret-right'
            onClick={() => nextPhoto(index + 1)}
          ></i>
        </div>
      );
    }
    if (index > 0) {
      items.push(
        <div className='left-arrow' key='2'>
          <i
            className='fas fa-caret-left'
            onClick={() => nextPhoto(index - 1)}
          ></i>
        </div>
      );
    }
    return <div className='arrows'>{items}</div>;
  };

  const closePreview = () => {
    document.body.style.overflow = '';
    setOpen(false);
  };

  return (
    <div className='preview-background'>
      <i className='fas fa-times' onClick={() => closePreview()}></i>
      <div className='preview-container'>
        <img className='photo' src={url}></img>
        {checkURL()}
        <p className='comment'>{comment}</p>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  photos: state.city.city.photos,
});

export default connect(mapStateToProps, null)(PhotoPreview);
