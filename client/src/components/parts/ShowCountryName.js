import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import './ShowCountryName.scss';

const ShowCountryName = ({ showName, areaName }) => {
  return (
    <div className={classNames('show-countryname', `bar-${areaName}`)}>
      <p className='countyname'>{showName}</p>
    </div>
  );
};

ShowCountryName.propTypes = {
  showName: PropTypes.string,
  areaName: PropTypes.string,
};

export default ShowCountryName;
