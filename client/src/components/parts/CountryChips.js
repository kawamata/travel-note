import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './CountryChips.scss';
import CountryCard from '../parts/CountryCard';
import ShowCountryName from '../parts/ShowCountryName';
import Spinner from '../layout/Spinner';
import AddCountry from '../forms/AddCountry';
import {
  getCountries,
  addCountries,
  clearCountries,
} from '../../actions/countries';
import allAreas from '../common/allAreas';

const mapStateToProps = (state) => ({
  countries: state.countries,
});
const mapDispatchToProps = (dispatch) => ({
  getCountries() {
    return dispatch(getCountries());
  },
  addCountries(formData) {
    return dispatch(addCountries(formData));
  },
  clearCountries() {
    return dispatch(clearCountries());
  },
});

export class CountryChips extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      withoutCountryTotal: [],
      restCountryTotal: [],
      showName: '',
      areaName: '',
      tooltipOpen: false,
      countryname: '',
      areaname: '',
      countryID: '',
      processDone: false,
      addCountryModalOpen: false,
      isPhone: false,
    };

    this.openTooltip = this.openTooltip.bind(this);
    this.postNewCountry = this.postNewCountry.bind(this);
    this.addCountryModal = this.addCountryModal.bind(this);

    this.primary();
  }

  componentDidMount() {
    this.isSmartPhone();
  }

  primary() {
    // ログアウト=>別アカウントでログイン時に前のデータをクリアする
    this.props.clearCountries().then(() => {
      this.props.getCountries().then(() => {
        const {
          countries,
          countries: { withCountries, noCountries, loading },
        } = this.props;
        if (countries && !loading && withCountries && noCountries) {
          this.organizeCountries();
        }
      });
    });
  }

  organizeCountries() {
    // 前回の描画過程の更新をリセット
    this.setState({ processDone: false });

    const {
      countries: { withCountries, noCountries },
    } = this.props;
    const { withoutCountryTotal, restCountryTotal } = this.state;
    // 国がないareaの合計国数を配列にする（例：[oceania: 14, middle_east: 37]）
    if (noCountries.length > 0) {
      for (let area in allAreas) {
        Object.keys(noCountries).forEach((country) => {
          if (area === noCountries[country]) {
            withoutCountryTotal[area] = allAreas[area];
          }
        });
      }
    }

    // 国が存在するareaで国が無い数を出し配列にする（例：[asia: 19, europe: 46]）
    if (Object.keys(withCountries).length > 0) {
      for (let area in allAreas) {
        Object.keys(withCountries).forEach((country) => {
          if (area === country) {
            const rest =
              Number(allAreas[area]) - Number(withCountries[country]);
            restCountryTotal[area] = rest;
          }
        });
      }
    }

    // propsの値の有無を条件式で判断できない為、この過程の完了をstateで判断できるようにする
    this.setState({ processDone: true });
  }

  onClick(country, area) {
    this.setState({
      countryname: country,
      areaname: area,
    });
    this.openTooltip(true, country);
    this.getCountryId(country);
  }
  lockBackground(bool) {
    if (bool === true) {
      document.body.setAttribute('style', 'overflow: hidden');
    } else {
      document.body.setAttribute('style', 'overflow: none');
    }
  }
  openTooltip(bool) {
    this.setState({ tooltipOpen: bool });
    this.lockBackground(bool);
  }
  getCountryId(countryname) {
    const {
      countries: { countries },
    } = this.props;
    countries.forEach((country) => {
      if (country.countryname === countryname) {
        this.setState({ countryID: country._id });
      }
    });
  }
  showCountryName(countryname, area) {
    this.setState({
      showName: countryname,
      areaName: area,
    });
  }
  hideCountryName() {
    this.setState({ showName: '' });
  }

  // addCountryモーダル開閉
  addCountryModal(bool) {
    this.setState({ addCountryModalOpen: bool });
    this.lockBackground(bool);
  }

  // addCountryのpostリクエスト
  postNewCountry(formData) {
    this.props
      .addCountries(formData)
      .then(() => {
        const {
          countries: { add_done },
        } = this.props;
        // addCountryの完了後に再描画
        if (add_done) {
          this.primary();
        }
      })
      .then(() => {
        this.addCountryModal(false);
      });
  }

  // スマホ判定
  isSmartPhone() {
    if (navigator.userAgent.match(/iPhone|Android.+Mobile/)) {
      this.setState({ isPhone: true });
    }
  }

  render() {
    const {
      countries: { withCountries, countries },
    } = this.props;
    const {
      restCountryTotal,
      withoutCountryTotal,
      showName,
      areaName,
      tooltipOpen,
      countryname,
      areaname,
      countryID,
      processDone,
      addCountryModalOpen,
      isPhone,
    } = this.state;

    // chipを描写
    const chips = (area, note) => {
      const items = [];

      // 国なしareaのchips描写
      if (note === 'without') {
        for (let i = 1; i <= withoutCountryTotal[area]; i++) {
          items.push(
            <Link to='#' key={`${i}-without`}>
              <li className={classNames('chip', 'gray', `chip-${area}`)}></li>
            </Link>
          );
        }
        return <ul>{items}</ul>;
      }

      // 国ありareaのchip描写
      // 国あり描写
      if (note === 'with') {
        Object.keys(withCountries).forEach((countryArea) => {
          if (area === countryArea) {
            Object.keys(countries).forEach((country) => {
              const countryname = countries[country].countryname;
              if (area === countries[country].area.toLowerCase()) {
                items.push(
                  <Fragment key={`${countryname}`}>
                    <Link to='#' key={`${countryname}`}>
                      <li
                        className={classNames(
                          'chip',
                          'color',
                          `chip-${area}`,
                          `${countries[country].countryname}`
                        )}
                        onMouseOver={(e) =>
                          this.showCountryName(
                            countries[country].countryname,
                            area
                          )
                        }
                        onMouseOut={() => this.hideCountryName()}
                        onClick={(e) =>
                          this.onClick(countries[country].countryname, area)
                        }
                      ></li>
                    </Link>
                  </Fragment>
                );
              }
            });
          }
        });
        // 国なし描写
        for (let i = 1; i <= restCountryTotal[area]; i++) {
          items.push(
            <Link to='#' key={`${i}-with`}>
              <li className={classNames('chip', 'gray', `chip-${area}`)}></li>
            </Link>
          );
        }
        return <ul>{items}</ul>;
      }
    };

    // 国なしareaを作成
    const noCountriesElement = Object.keys(withoutCountryTotal).map((area) => (
      <div
        className={classNames('area-inner', `${area}`)}
        key={`${area}-without`}
      >
        <div className='area-title'>{area}</div>
        <hr />
        {chips(area, 'without')}
      </div>
    ));

    // 国ありareaを作成
    const withCountriesElement = Object.keys(restCountryTotal).map((area) => (
      <div className={classNames('area-inner', `${area}`)} key={`${area}-with`}>
        <div className='area-title'>{area}</div>
        <hr />
        {chips(area, 'with')}
      </div>
    ));

    return !processDone ? (
      <Spinner />
    ) : (
      <div className='areas'>
        {addCountryModalOpen && (
          <AddCountry
            setOpen={this.addCountryModal}
            addCountryAction={this.postNewCountry}
          />
        )}
        <div className='add'>
          <div
            className='add-new-btn'
            onClick={() => this.addCountryModal(true)}
          >
            <div>
              <p>
                Add New
                <br />
                Country
              </p>
            </div>
          </div>
        </div>

        {tooltipOpen && countryID && (
          <CountryCard
            countryname={countryname}
            areaname={areaname}
            openTooltip={this.openTooltip}
            country_id={countryID}
          />
        )}
        <div className='without-areas'>{noCountriesElement}</div>
        <div className='with-areas'>{withCountriesElement}</div>
        <div className='show-countryname-container'>
          {showName.length && !isPhone && (
            <ShowCountryName showName={showName} areaName={areaName} />
          )}
        </div>
      </div>
    );
  }
}

CountryChips.propTypes = {
  countries: PropTypes.object,
  byArea: PropTypes.array,
  noCountries: PropTypes.array,
};

export default connect(mapStateToProps, mapDispatchToProps)(CountryChips);
