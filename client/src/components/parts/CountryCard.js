import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import './CountryCard.scss';

import Button from './Button';
import { getCities } from '../../actions/countries';

const CountryCard = ({
  countryname,
  areaname,
  openTooltip,
  getCities,
  country_id,
  cities,
}) => {
  useEffect(() => {
    getCities(country_id);
  }, [getCities]);

  const citiesElement = () => {
    if (cities.length > 0) {
      const ciryArray = [];
      cities.forEach((city) => {
        ciryArray.unshift(city.cityname);
      });
      return <p className='description'>{ciryArray.join(', ')}</p>;
    } else {
      return (
        <p className='description'>
          You haven't added cities for {countryname}.
          <br />
          For adding cities, go to See More.
        </p>
      );
    }
  };

  return (
    <div className='countrycard-background'>
      <div className='countrycard-container'>
        <div className='countrycard-inner'>
          <i className='fas fa-times' onClick={() => openTooltip(false)}></i>
          <h4 className='countryname'>{countryname}</h4>
          <small className='areaname'>{areaname}</small>
          <hr />
          <p className='subject'>Cities</p>
          {cities && citiesElement()}
          <Link to={`/country/${country_id}`}>
            <Button color='blue' size='s' text='See More' />
          </Link>
        </div>
      </div>
    </div>
  );
};

CountryCard.propTypes = {
  openTooltip: PropTypes.func.isRequired,
  countryname: PropTypes.string.isRequired,
  areaname: PropTypes.string.isRequired,
  getCities: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  cities: state.cities.cities,
});

export default connect(mapStateToProps, { getCities })(CountryCard);
