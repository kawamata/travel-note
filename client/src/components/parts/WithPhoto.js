import React, { useState } from 'react';
import classNames from 'classnames';

const WithPhoto = (city) => {
  const [shownote, setNote] = useState();
  const showNote = (e) => {
    e.stopPropagation();
    setNote(true);
  };
  const hideNote = (e) => {
    e.stopPropagation();
    setNote(false);
  };

  return (
    <li className='withphoto'>
      <div
        className='withphoto-inner'
        onMouseEnter={(e) => showNote(e)}
        onMouseLeave={(e) => hideNote(e)}
      >
        <div
          className={classNames('show-note', {
            'is-visible': shownote,
          })}
        >
          <p>{city.city.photos[0].note}</p>
        </div>
        <img src={city.city.photos[0].photo} />
      </div>
    </li>
  );
};

export default WithPhoto;
