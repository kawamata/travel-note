import React, { useState, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router-dom';

import './CityCard.scss';

import PhotoUploader from '../forms/PhotoUploader';
import WithPhoto from './WithPhoto';

const CityCard = ({ cities, area, country_id }) => {
  const areaname = area.toLowerCase();
  // 日付
  const showDate = (date) => {
    const moment = require('moment');
    // const date = moment().format('MM/DD/YYYY');
    return moment(date).format('MM/DD/YYYY');
  };

  const [open, setOpen] = useState();
  const [citydata, setCityData] = useState({
    cityname: '',
    cityid: '',
    countryid: '',
  });
  const onOpen = (cityname, cityid) => {
    setCityData({ cityname: cityname, cityid: cityid, countryid: country_id });
    setOpen(true);
  };

  const items = [];
  const cardElement = () => {
    for (let i = 0; i < Object.keys(cities).length; i++) {
      items.push(
        <div key={i} className='citycard'>
          <ul className='citycard-contents'>
            <li className='cityname'>{cities[i].cityname}</li>
            <Link
              to={{
                pathname: `/city/${cities[i]._id}`,
                state: { country_id: country_id },
              }}
            >
              <i className='fas fa-plus-circle'></i>
            </Link>
            <li className='date'>
              {cities[i].visitdate
                ? showDate(cities[i].visitdate)
                : 'No date added'}
            </li>
            <li className='note'>
              {cities[i].notes.length ? cities[i].notes[0].note : ''}
            </li>
            {cities[i].photos.length === 0 ? (
              <li
                className='photo'
                onClick={(e) => onOpen(cities[i].cityname, cities[i]._id)}
              >
                <i className='fas fa-camera'></i>
              </li>
            ) : (
              <WithPhoto city={cities[i]} />
            )}
          </ul>
          <div
            className={classNames('rightcolor', `citycard-${areaname}`)}
          ></div>
        </div>
      );
    }
    return <div className='citycard-wrapper'>{items}</div>;
  };

  return (
    <Fragment>
      {open && (
        <PhotoUploader setOpen={setOpen} citydata={citydata} first={true} />
      )}
      <div className='citycard-container'>
        {Object.keys(cities).length > 0 ? (
          cardElement()
        ) : (
          <p className='nocities'>You haven't added cities.</p>
        )}
      </div>
    </Fragment>
  );
};

const mapStateToProps = (state) => ({
  area: state.country.country.area,
  cities: state.country.country.cities,
  country_id: state.country.country._id,
});

CityCard.propTypes = {
  cities: PropTypes.array.isRequired,
  area: PropTypes.string.isRequired,
};

export default connect(mapStateToProps, null)(CityCard);
