import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Alert from '../layout/Alert';
import Register from '../auth/Register';
import Login from '../auth/Login';
import Top from '../pages/Top';
import Country from '../pages/Country';
import City from '../pages/City';

const Routes = () => {
  return (
    <section className='container'>
      <Alert />
      <Switch>
        <Route exact path='/register' component={Register} />
        <Route exact path='/login' component={Login} />
        <Route exact path='/top' component={Top} />
        <Route exact path='/country/:country_id' component={Country} />
        <Route exact path='/city/:city_id' component={City} />
      </Switch>
    </section>
  );
};

export default Routes;
