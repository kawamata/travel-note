import React, { useState } from 'react';
import { useDropzone } from 'react-dropzone';
import { connect } from 'react-redux';
import classNames from 'classnames';

import { postPhotos } from '../../actions/photos';

import './PhotoUploader.scss';

const PhotoUploader = ({
  setOpen,
  citydata: { cityname, cityid, countryid },
  postPhotos,
  username,
  first,
  isCity,
}) => {
  const user = username.toLowerCase();

  // commentを取得
  const [formData, setComment] = useState({ comment: '' });
  const { comment } = formData;
  const onChange = (e) => {
    setComment({ ...formData, [e.target.name]: e.target.value });
  };

  const onDrop = (acceptedFiles) => {
    postPhotos(acceptedFiles, user, countryid, cityid, comment, isCity).then(
      setOpen(false)
    );
  };
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  return (
    <div className='uploader-container'>
      <div className='uploader-frame'>
        <i className='fas fa-times' onClick={(e) => setOpen(false)}></i>
        <div className='text'>
          {first ? (
            <p>
              Upload your first photo for <span>{cityname}</span>.
            </p>
          ) : (
            <p>
              Upload photos for <span>{cityname}</span>.
            </p>
          )}
          <hr />
          <label>Add comment for photos you wish to upload.</label>
          <textarea
            className='comment'
            type='text'
            placeholder=''
            name='comment'
            value={comment}
            onChange={(e) => onChange(e)}
          />
          <p className={classNames('dropzone-description')}>
            JPG/PNG files only. You need to add comment before uploading photos.
          </p>
        </div>
        <div
          {...getRootProps()}
          className={classNames('dropzone', {
            'is-disabled': comment === '',
          })}
        >
          <input {...getInputProps()} />
          {comment === '' ? (
            <p className='add-comment-first'>
              Please add comment before uploading photos.
            </p>
          ) : isDragActive ? (
            <p>Drop a photo here</p>
          ) : (
            <p>
              Drag and Drop
              <br />
              or
              <br /> Click here
            </p>
          )}
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  username: state.auth.user.name,
});

PhotoUploader.defaultProps = {
  isCity: false,
  first: false,
};

export default connect(mapStateToProps, { postPhotos })(PhotoUploader);
