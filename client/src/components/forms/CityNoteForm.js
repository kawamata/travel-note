import React, { useState } from 'react';

import './CityNoteForm.scss';
import Button from '../parts/Button';

const CityNoteForm = ({ setOpen, cityAction, cityname, noteValue }) => {
  const [note, setNote] = useState({ note: '' });
  const onSubmit = () => {
    cityAction(note);
    setOpen(false);
  };

  const onChange = (e) => {
    setNote(e.target.value);
  };

  return (
    <div className='citynote-background'>
      <div className='citynote-frame'>
        <i className='fas fa-times' onClick={() => setOpen(false)}></i>
        <div className='text'>
          <p>
            Add Note for <span>{cityname}</span>
          </p>
          <hr />
          <textarea
            className='note'
            type='text'
            placeholder=''
            name='note'
            onChange={(e) => onChange(e)}
            required
            defaultValue={noteValue && noteValue}
          />
          <div className='submit' onClick={(e) => onSubmit(e)}>
            <Button color='blue' size='s' text='Submit Note' />
          </div>
        </div>
      </div>
    </div>
  );
};

export default CityNoteForm;
