import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classNames from 'classnames';
import './AddCountry.scss';

import Button from '../parts/Button';
import areas from './areas.json';
import allAreas from '../common/allAreas';

const AddCountry = ({ setOpen, addCountryAction, history, withCountries }) => {
  const [formData, setFormData] = useState({ countryname: '', area: '' });
  const { countryname, area } = formData;

  const areaDropdown = () => {
    const areaSelect = [
      <option className='choose' key='choose'>
        Choose Area
      </option>,
    ];
    areas.map((area) => {
      areaSelect.push(<option key={area}>{area}</option>);
    });
    return (
      <select
        className='area-select'
        value={area}
        name='area'
        onChange={(e) => onChange(e)}
      >
        {areaSelect}
      </select>
    );
  };

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = () => {
    // 国数が埋まってないかバリデーション
    checkCountryOccupancy(formData.area);
    addCountryAction(formData);
  };

  // 国数チェック
  const [errorMsg, setErrorMsg] = useState(false);
  const checkCountryOccupancy = (area) => {
    if (!(withCountries[area] && withCountries[area] < allAreas[area])) {
      // エラーメッセージ表示
      setErrorMsg(true);
      return false;
    }
  };

  return (
    <div className={classNames('add-country-background')}>
      <div className='add-country-container'>
        <div className='add-country-inner'>
          <i className='fas fa-times' onClick={() => setOpen(false)}></i>
          <h3 className='country-title'>Add New Country</h3>
          {errorMsg && (
            <p className='error-msg'>
              No more country can be added in this area.
            </p>
          )}
          <p className='description'>
            To create a new country, please enter required information.
          </p>
          <div>
            <input
              className='input-field'
              type='text'
              placeholder='Country Name'
              name='countryname'
              value={countryname}
              onChange={(e) => onChange(e)}
              required
            />
            <div>{areaDropdown()}</div>
            <div className='submit' onClick={(e) => onSubmit(e)}>
              <Button
                type='submit'
                size='m'
                color='orange'
                text='Add Country'
                className='btn'
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  withCountries: state.countries.withCountries,
});

export default connect(mapStateToProps, null)(AddCountry);
