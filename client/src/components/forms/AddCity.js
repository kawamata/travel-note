import React, { useState } from 'react';
import { connect } from 'react-redux';

import './AddCity.scss';

import Button from '../parts/Button';
import { addCity } from '../../actions/country';
import { getCountry } from '../../actions/country';

const AddCity = ({ addCitysetOpen, country_id, addCity, getCountry }) => {
  const [formData, setFormData] = useState({
    cityname: '',
    visitdate: '',
    note: '',
  });
  const { cityname, visitdate, note } = formData;

  const onChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    addCity(formData, country_id).then(closeModal(false));
  };

  const closeModal = (bool) => {
    addCitysetOpen(bool);
  };

  return (
    <div className='addcity-background'>
      <div className='addcity-container'>
        <div className='addcity-inner'>
          <i className='fas fa-times' onClick={(e) => closeModal(false)}></i>
          <h3 className='city-title'>Add New City</h3>
          <p className='description'>
            To create a new city, please enter required information.
          </p>
          <form onSubmit={(e) => onSubmit(e)}>
            <label>
              City Name<span>&nbsp;&nbsp;*required</span>
            </label>
            <input
              className='input-field'
              type='text'
              placeholder='City Name'
              name='cityname'
              value={cityname}
              onChange={(e) => onChange(e)}
              required
            />
            <label>
              Visit Date<span>&nbsp;&nbsp;*required</span>
            </label>
            <input
              className='input-field'
              type='date'
              name='visitdate'
              value={visitdate}
              onChange={(e) => onChange(e)}
              required
            />
            <label>Note</label>
            <textarea
              className='input-field'
              type='text'
              name='note'
              value={note}
              onChange={(e) => onChange(e)}
            />
            <Button
              type='submit'
              size='m'
              color='orange'
              text='Add City'
              className='btn'
            />
          </form>
        </div>
      </div>
    </div>
  );
};

export default connect(null, { addCity, getCountry })(AddCity);
