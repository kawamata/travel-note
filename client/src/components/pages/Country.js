import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';

import './Country.scss';
import Spinner from '../layout/Spinner';
import Button from '../parts/Button';
import CityCard from '../parts/CityCard';
import AddCity from '../forms/AddCity';
import Modal from '../parts/Modal';

import {
  getCountry,
  deleteCountry,
  getAllCountries,
} from '../../actions/country';

const Country = ({
  country: { country },
  countries,
  deleteCountry,
  getAllCountries,
  ...props
}) => {
  const history = useHistory();

  const country_id = props.match.params.country_id;
  useEffect(() => {
    props.getCountry(country_id);
  }, [getCountry]);

  useEffect(() => {
    getAllCountries();
  }, [getAllCountries]);

  // 前のページの背景スクロールコントロールを解除
  useEffect(() => {
    document.body.setAttribute('style', 'overflow: none');
  }, []);

  // モーダル背景固定
  const lockBackground = (bool) => {
    if (bool === true) {
      document.body.setAttribute('style', 'overflow: hidden');
    } else {
      document.body.setAttribute('style', 'overflow: none');
    }
  };
  const [open, setOpen] = useState(false);
  const addCitysetOpen = (bool) => {
    lockBackground(bool);
    setOpen(bool);
  };

  // Country削除
  const [deleteCountryModal, setDeleteCountryModal] = useState(false);
  const toggleDeleteCountry = (bool) => {
    lockBackground(bool);
    setDeleteCountryModal(bool);
  };
  // 関数をModalに渡してyes押下で戻す(Country削除)
  const removeCounry = () => {
    deleteCountry(country_id).then(history.push('/top'));
  };

  const items = [];
  const allCountries = () => {
    let path;
    for (let i = 0; i < Object.keys(countries).length; i++) {
      if (countries[i].countryname !== country.countryname) {
        path = `/country/${countries[i]._id}/`;
        items.push(
          <a href={path} className='othercountryname' key={i}>
            {countries[i].countryname}
          </a>
        );
      }
    }
    return <div className='othercountryname-outer'>{items}</div>;
  };

  return country === null ? (
    <Spinner />
  ) : (
    <div className='country-container'>
      {deleteCountryModal && (
        <Modal
          onClose={toggleDeleteCountry}
          name={country.countryname}
          deleteAction={removeCounry}
        />
      )}
      <h3 className='countryname'>{country.countryname}</h3>
      <hr size='1' />
      <CityCard country_id={country_id} />
      <div onClick={() => addCitysetOpen(true)} className='btn-outer'>
        <Button color='blue' size='s' text='Add City' />
      </div>
      {open && (
        <AddCity addCitysetOpen={addCitysetOpen} country_id={country_id} />
      )}
      <hr size='1' className='delete-city-hr' />
      <div
        className='delete-country-btn'
        onClick={() => toggleDeleteCountry(true)}
      >
        <Button color='red-frame' size='xs' text='Delete Country' />
      </div>
      {Object.keys(countries).length > 1 && (
        <div className='other-countries'>
          <div className='other-countries-wapper'>
            <h3 className='other-countries-title'>See Other Countries</h3>
            {allCountries()}
          </div>
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state) => ({
  country: state.country,
  countries: state.country.countries,
});

Country.propTypes = {
  country: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, {
  getCountry,
  deleteCountry,
  getAllCountries,
})(Country);
