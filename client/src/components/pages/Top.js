import React from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Spinner from '../layout/Spinner';
import CountryChips from '../parts/CountryChips';

import './Top.scss';

const Top = ({ auth: { user, isAuthenticated } }) => {
  if (!isAuthenticated) {
    return <Redirect to='/' />;
  }

  return !user ? (
    <Spinner />
  ) : (
    <div className='top'>
      <div className='background'></div>
      <div className='title'>
        <i className='fas fa-2x fa-plane'></i>
        <p className='name'>Hi {user && user.name}</p>
        <p className='description'>
          Collect countries where you've traveld and record memories.
        </p>
      </div>
      <CountryChips />
    </div>
  );
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

Top.propTypes = {
  auth: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, null)(Top);
