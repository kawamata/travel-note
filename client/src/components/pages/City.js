import React, { useEffect, useState, Fragment } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import moment from 'moment';
import classNames from 'classnames';
import { Link } from 'react-router-dom';

import './City.scss';
import {
  getCity,
  deletePhoto,
  deleteNote,
  deleteCity,
  addCityNote,
  editNote,
  getThisCountry,
} from '../../actions/city';

import Spinner from '../layout/Spinner';
import Button from '../parts/Button';
import PhotoUploader from '../forms/PhotoUploader';
import CityNoteForm from '../forms/CityNoteForm';
import PhotoPreview from '../parts/PhotoPreview';
import Modal from '../parts/Modal';

const City = ({
  getThisCountry,
  getCity,
  deletePhoto,
  deleteNote,
  deleteCity,
  addCityNote,
  editNote,
  loading,
  city,
  country,
  ...props
}) => {
  const history = useHistory();

  const city_id = props.match.params.city_id;
  const country_id = props.location.state.country_id;
  useEffect(() => {
    getThisCountry(country_id);
  }, [getThisCountry]);
  useEffect(() => {
    getCity(country_id, city_id);
  }, [getCity]);

  const path = `/country/${country && country._id}`;

  // 写真削除
  const [photoMinus, setPhotoMinus] = useState(false);
  const togglePhoto = () => {
    setPhotoMinus(!photoMinus);
  };
  const onDeletePhoto = (photo_id) => {
    setPhotoMinus(!photoMinus);
    deletePhoto(country_id, city_id, photo_id);
  };

  // 写真表示
  const showPhotos = () => {
    const items = [];
    if (city.photos.length > 0) {
      city.photos.map((photo) => {
        items.push(
          <span key={photo._id}>
            <img
              src={photo.photo}
              key={photo._id}
              className='photo'
              onClick={() => openPhotoPreview(photo.photo, photo.note)}
            />
            {photoMinus && (
              <i
                className='fas fa-minus-circle photo-minus'
                onClick={() => onDeletePhoto(photo._id)}
              ></i>
            )}
          </span>
        );
      });
      return <div className='photo-wrapper'>{items}</div>;
    }
  };
  // 写真アップロード
  const [citydata, setCityData] = useState({
    cityname: '',
    cityid: '',
    countryid: '',
  });
  const [openUpload, setOpenUpload] = useState(false);
  const setUploadModalToggle = (bool) => {
    setOpenUpload(bool);
    lockBackground(bool);
  };
  const onAddPhotos = (bool) => {
    setCityData({
      cityname: city.cityname,
      cityid: city._id,
      countryid: country_id,
    });
    setUploadModalToggle(bool);
  };

  // 写真プレビュー
  const [openPreview, setOpenPreview] = useState(false);
  const [photoData, setPhotoData] = useState({ photo: '', comment: '' });
  const openPhotoPreview = (photo, comment) => {
    setPhotoData({ photoUrl: photo, photoComment: comment });
    setOpenPreview(true);
    document.body.style.overflow = 'hidden';
  };

  // noteを表示
  const showNotes = () => {
    const items = [];
    if (city.notes.length > 0) {
      city.notes.map((note) => {
        items.push(
          <li className='note-inner' key={note._id}>
            <div className='left-wrapper'>
              {/* 削除 */}
              <i
                className={classNames('fas fa-minus-circle', 'note-icon', {
                  'is-visible': noteMinus === true,
                })}
                onClick={() => onDeleteNote(note._id)}
              ></i>
              {/* 編集 */}
              <i
                className={classNames('fas fa-pencil-alt', 'note-icon', {
                  'is-visible': noteEdit === true,
                })}
                onClick={() => onEditNote(note._id, note.note)}
              ></i>
              <span className='note'>{note.note}</span>
            </div>
            <span className='note-date'>
              {moment(note.date).format('MM/DD/YYYY')}
            </span>
          </li>
        );
      });
      return <ul className='notes-wrapper'>{items}</ul>;
    }
  };
  // note追加
  const [openAddNote, setOpenAddNote] = useState(false);
  const addNote = (bool) => {
    lockBackground(bool);
    setOpenAddNote(bool);
  };
  const addNoteToCity = (note) => {
    addCityNote(country_id, city_id, note);
  };

  // note削除
  const [noteMinus, setNoteMinus] = useState(false);
  const toggleNoteDelete = () => {
    setNoteMinus(!noteMinus);
  };
  const onDeleteNote = (note_id) => {
    deleteNote(country_id, city_id, note_id);
    setNoteMinus(!noteMinus);
  };

  // note編集
  const [openEditNote, setOpenEditNote] = useState(false);
  const [noteEdit, setNoteEdit] = useState(false);
  const [noteId, setNoteId] = useState('');
  const [noteValue, setNoteValue] = useState('');

  const toggleNoteEdit = () => {
    setNoteEdit(!noteEdit);
  };
  const onEditNote = (note_id, note) => {
    setNoteId(note_id);
    setOpenEditNote(!openEditNote);
    setNoteValue(note);
  };
  const editCityNote = (note) => {
    editNote(country_id, city_id, note, noteId).then(toggleNoteEdit());
  };

  // モーダル背景固定
  const lockBackground = (bool) => {
    if (bool === true) {
      document.body.setAttribute('style', 'overflow: hidden');
    } else {
      document.body.setAttribute('style', 'overflow: none');
    }
  };
  // City削除
  const [deleteCityModal, setDeleteCityModal] = useState(false);
  const toggleDeleteCity = (bool) => {
    lockBackground(bool);
    setDeleteCityModal(bool);
  };
  // 関数をModalに渡してyes押下で戻す(Country削除)
  const removeCity = () => {
    deleteCity(country_id, city_id).then(
      history.push(`/country/${country_id}`)
    );
  };

  return !city ? (
    <Spinner />
  ) : (
    <Fragment>
      {deleteCityModal && (
        <Modal
          onClose={toggleDeleteCity}
          name={city.cityname}
          deleteAction={removeCity}
        />
      )}
      {openPreview && (
        <PhotoPreview setOpen={setOpenPreview} photoData={photoData} />
      )}
      <div className='city-container'>
        {openUpload && (
          <PhotoUploader
            citydata={citydata}
            setOpen={setUploadModalToggle}
            isCity={true}
          />
        )}
        {openAddNote && (
          <CityNoteForm
            cityAction={addNoteToCity}
            setOpen={addNote}
            cityname={city.cityname}
          />
        )}
        {openEditNote && (
          <CityNoteForm
            cityAction={editCityNote}
            setOpen={setOpenEditNote}
            cityname={city.cityname}
            noteValue={noteValue}
          />
        )}
        <Link to={path} className='countryname'>
          {country && country.countryname}
        </Link>
        <h3 className='cityname'>{city.cityname}</h3>

        <hr size='1' />
        <p className='visitdate'>
          {moment(city.visitdate).format('MM/DD/YYYY')}
        </p>
        {showPhotos()}
        <div className='buttons'>
          <div onClick={() => onAddPhotos(true)}>
            <Button color='blue' size='xs' text='Add Photos' />
          </div>
          {city.photos.length > 0 && (
            <div className='btn-wrapper' onClick={togglePhoto}>
              {photoMinus ? (
                <Button color='gray' size='xs' text='Done' />
              ) : (
                <Button color='gray' size='xs' text='Delete Photo' />
              )}
            </div>
          )}
        </div>
        <div className='notes'>
          <h4>Notes</h4>
          <hr size='1' />
          {showNotes()}
          <div className='clear'></div>
          <div className='buttons btn-notes'>
            <div onClick={() => addNote(true)}>
              <Button color='blue' size='xs' text='Add Note' />
            </div>
            {city.notes.length > 0 && (
              <div className='btn-outer'>
                <div
                  className='btn-wrapper lower edit-note'
                  onClick={toggleNoteEdit}
                >
                  <Button color='green' size='xs' text='Edit Note' />
                </div>
                <div className='btn-wrapper lower' onClick={toggleNoteDelete}>
                  {noteMinus ? (
                    <Button color='gray' size='xs' text='Done' />
                  ) : (
                    <Button color='gray' size='xs' text='Delete Note' />
                  )}
                </div>
              </div>
            )}
          </div>
        </div>
        <hr size='1' />
        <div className='delete-city-btn' onClick={() => toggleDeleteCity(true)}>
          <Button color='red-frame' size='xs' text='Delete City' />
        </div>
      </div>
    </Fragment>
  );
};

const mapStateToProps = (state) => ({
  city: state.city.city,
  loading: state.city.loading,
  country: state.city.country,
});

const mapDispatchToProps = {
  getCity,
  deletePhoto,
  deleteNote,
  deleteCity,
  addCityNote,
  editNote,
  getThisCountry,
};

export default connect(mapStateToProps, mapDispatchToProps)(City);
