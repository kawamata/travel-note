import firebase from 'firebase/app';
import 'firebase/storage';

const firebaseConfig = {
  apiKey: 'AIzaSyDsZBQRjqts8H9GXPvowS2xjlN8Ytt_U-g',
  authDomain: 'travel-note-84771.firebaseapp.com',
  databaseURL: 'https://travel-note-84771.firebaseio.com',
  projectId: 'travel-note-84771',
  storageBucket: 'travel-note-84771.appspot.com',
  messagingSenderId: '758452559484',
  appId: '1:758452559484:web:5c989ea1743fde0c8ff3f9',
  measurementId: 'G-H440XRSXV4',
};

firebase.initializeApp(firebaseConfig);

export const storage = firebase.storage();
export default firebase;
