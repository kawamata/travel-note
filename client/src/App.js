import React, { Fragment, useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import setAuthToken from './utils/setAuthToken';
import { loadUser } from './actions/auth';

import Navbar from './components/layout/Navbar';
import Landing from './components/layout/Landing';
import Routes from './components/routing/Routes';

import './style/App.css';

// Redux
import { Provider } from 'react-redux';
import store from './store';

// tokenがあれば、headerに格納
if (localStorage.token) {
  setAuthToken(localStorage.token);
}

const App = () => {
  // ページ読み込み後にユーザー情報をstoreに格納
  // 常に新しいユーザー情報をstoreに保持できる
  useEffect(() => {
    store.dispatch(loadUser());
  }, []);

  return (
    <Provider store={store}>
      <Router>
        <Fragment>
          <Fragment>
            <Navbar />
            <Switch>
              <Route exact path='/' component={Landing} />
              <Route component={Routes} />
            </Switch>
          </Fragment>
        </Fragment>
      </Router>
    </Provider>
  );
};

export default App;
