import { combineReducers } from 'redux';

import alert from './alert';
import auth from './auth';
import countries from './countries';
import cities from './cities';
import country from './country';
import city from './city';

export default combineReducers({
  alert,
  auth,
  countries,
  cities,
  country,
  city,
});
