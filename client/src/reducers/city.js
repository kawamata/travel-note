import {
  GET_CITY,
  GET_CITY_ERROR,
  ADD_PHOTOURL_CITY,
  PHOTOURL_ERROR,
  ADD_CITYNOTE,
  CITYNOTE_ERROR,
  EMPTY_CITY,
  DELETE_PHOTO,
  DELETE_PHOTO_ERROR,
  DELETE_NOTE,
  DELETE_NOTE_ERROR,
  EDIT_NOTE,
  EDIT_NOTE_ERROR,
  GET_THISCOUNTRY,
  THISCOUNTRY_ERROR,
} from '../actions/types';

const initialState = {
  city: null,
  loading: true,
  error: {},
};

export default function (state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case GET_CITY:
    case ADD_PHOTOURL_CITY:
    case ADD_CITYNOTE:
    case DELETE_PHOTO:
    case DELETE_NOTE:
    case EDIT_NOTE:
      return {
        ...state,
        city: payload,
        loading: false,
      };

    case GET_CITY_ERROR:
    case EMPTY_CITY:
      return {
        ...state,
        city: null,
        error: payload,
        loading: false,
      };
    case PHOTOURL_ERROR:
    case CITYNOTE_ERROR:
    case DELETE_PHOTO_ERROR:
    case DELETE_NOTE_ERROR:
    case EDIT_NOTE_ERROR:
    case THISCOUNTRY_ERROR:
      return {
        ...state,
        error: payload,
        loading: false,
      };

    case GET_THISCOUNTRY:
      return {
        ...state,
        country: payload,
      };

    default:
      return state;
  }
}
