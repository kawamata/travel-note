import {
  GET_COUNTRY,
  COUNTRY_ERROR,
  ADD_PHOTOURL_COUNTRY,
  PHOTOURL_ERROR,
  ADD_CITY,
  CITY_ERROR,
  // GET_MYCOUNTRIES,
  DELETE_CITY,
  DELETE_CITY_ERROR,
  GET_ALLCOUNTRIES,
  COUNTRIES_ERROR,
} from '../actions/types';

const initialState = {
  country: null,
  loading: true,
  error: {},
  countries: [],
};

export default function (state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case GET_COUNTRY:
    case ADD_PHOTOURL_COUNTRY:
    case ADD_CITY:
    case DELETE_CITY:
      return {
        ...state,
        country: payload,
        loading: false,
      };
    case COUNTRY_ERROR:
      return {
        ...state,
        error: payload,
        loading: false,
        country: null,
      };

    case PHOTOURL_ERROR:
    case CITY_ERROR:
    case DELETE_CITY_ERROR:
      return {
        ...state,
        error: payload,
        loading: false,
      };
    // Topページに戻ったタイミングでCountryの中身を空にする
    // case GET_MYCOUNTRIES:
    //   return {
    //     ...state,
    //     country: null,
    //   };

    case GET_ALLCOUNTRIES:
      // sort as alphabetical order
      payload.sort((a, b) =>
        a.countryname !== b.countryname
          ? a.countryname < b.countryname
            ? -1
            : 1
          : 0
      );
      return {
        ...state,
        countries: payload,
      };
    case COUNTRIES_ERROR:
      return {
        ...state,
        countries: [],
      };

    default:
      return state;
  }
}
