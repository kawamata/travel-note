import {
  GET_MYCOUNTRIES,
  COUNTRIES_ERROR,
  ADD_COUNTRY,
  COUNTRY_ERROR,
  DELETE_COUNTRY,
  COUNTRY_DELETE_ERROR,
  CLEAR_USER_COUNTRIES,
} from '../actions/types';

const initialState = {
  countries: [],
  withCountries: [],
  noCountries: [],
  loading: true,
  error: {},
  add_done: null,
};

export default function (state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case GET_MYCOUNTRIES:
    case DELETE_COUNTRY:
      let countries = null;
      countries = payload;
      const areas = {
        oceania: 0,
        asia: 0,
        middle_east: 0,
        europe: 0,
        africa: 0,
        latin_america: 0,
        north_america: 0,
      };
      // areasをarea名のみを配列にする
      const allAreas = [];
      for (const area in areas) {
        allAreas.push(area);
      }
      // 国が存在するarea名のみを配列にする
      let countryExistAreas = [];
      countries.forEach((country) => {
        countryExistAreas.push(country.area.toLowerCase());
        countryExistAreas = countryExistAreas.filter(
          (x, i, self) => self.indexOf(x) === i
        );
      });

      // 国が無いareaのみの配列を作成（例： [oceania, middle_east]）
      const noCountries = [...countryExistAreas, ...allAreas].filter(
        (value) =>
          !countryExistAreas.includes(value) || !allAreas.includes(value)
      );

      // 国があるareaのみに国数を入れたオブジェクトを作成（例：{area: 2, europe: 3}）
      for (const area in areas) {
        countries.forEach((countryarea) => {
          if (area === countryarea.area.toLowerCase()) {
            areas[area]++;
          }
        });
      }
      for (const area in areas) {
        if (areas[area] === 0) {
          delete areas[area];
        }
      }

      return {
        ...state,
        withCountries: areas,
        noCountries: noCountries,
        loading: false,
        countries: countries,
      };

    case ADD_COUNTRY:
      return {
        ...state,
        add_done: true,
      };

    case COUNTRIES_ERROR:
    case COUNTRY_ERROR:
    case COUNTRY_DELETE_ERROR:
      return {
        ...state,
        error: payload,
        loading: false,
        countries: [],
        withCountries: [],
        noCountries: [],
      };

    case CLEAR_USER_COUNTRIES:
      return {
        ...state,
        countries: [],
        withCountries: [],
        noCountries: [],
        loading: false,
      };
    default:
      return state;
  }
}
