const path = require('path');
const express = require('express');
const connectDB = require('./config/db');
const app = express();

// productionで使用
const distDir = path.resolve(__dirname, './client/build');
const htmlFile = path.join(distDir, 'index.html');

// Connect Database
connectDB();

// セッション設定（developmentで使用）
if (process.env.NODE_ENV != 'production') {
  const cookieParser = require('cookie-parser');
  const session = require('express-session');
  const redis = require('redis');
  const RedisStore = require('connect-redis')(session);
  const redisClient = redis.createClient();
  app.use(cookieParser());
  app.use(
    session({
      secret: 'secret',
      resave: false,
      saveUninitialized: false,
      store: new RedisStore({ client: redisClient }),
      cookie: {
        httpOnly: true,
        secure: false,
        maxage: 1000 * 60 * 60,
      },
    })
  );
}

// Init Middleware
app.use(express.json({ extended: false }));

app.use('/api/users', require('./routes/api/users'));
app.use('/api/auth', require('./routes/api/auth'));
app.use('/api/countries', require('./routes/api/countries'));
app.use('/api/cities', require('./routes/api/cities'));
app.use('/api/country', require('./routes/api/country'));
app.use('/api/city', require('./routes/api/city'));

// app.getは、各apiのapp.useの後に配置のこと
if (process.env.NODE_ENV == 'production') {
  app.use(express.static(distDir));
  // productionの時
  app.get('*', (req, res) => {
    res.sendFile(htmlFile);
  });
} else {
  // developmentの時
  app.get('/', (req, res) => {
    // セッション設定
    res.json({
      sessionID: req.sessionID,
    });
    res.send('API Running...');
  });
}

// 開発環境では5000を使う：process.env.PORTが設定されてなければ5000
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
