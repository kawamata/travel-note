const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CountrySchema = new mongoose.Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'users',
  },
  countryname: {
    type: String,
    required: true,
  },
  area: {
    type: String,
    required: true,
  },
  cities: [
    {
      cityname: {
        type: String,
        require: true,
      },
      visitdate: {
        type: Date,
      },
      notes: [
        {
          note: {
            type: String,
          },
          date: {
            type: Date,
          },
        },
      ],
      photos: [
        {
          photo: {
            type: String,
          },
          note: {
            type: String,
          },
        },
      ],
    },
  ],
});

module.exports = Country = mongoose.model('Country', CountrySchema);
