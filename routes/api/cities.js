const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const auth = require('../../middleware/auth');
const moment = require('moment');

const Country = require('../../models/Country');

// @router GET Cities - api/cities
router.get('/:country_id', auth, async (req, res) => {
  try {
    const country = await Country.findById(req.params.country_id);
    const allcities = country.cities;
    if (!allcities) {
      return res.status(400).json({ msg: 'There is no cities added' });
    }
    res.json(allcities);
  } catch (err) {
    console.error(err);
    return res.status(500).json({ msg: 'Server error' });
  }
});

// @router POST City - api/cities/:country_id
router.post(
  '/:country_id',
  [auth, [check('cityname', 'City name is required').not().isEmpty()]],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      const date = moment().format('MM/DD/YYYY');

      const country = await Country.findById(req.params.country_id);
      const { cityname, note, visitdate } = req.body;
      const newCity = {
        cityname: cityname,
        visitdate: visitdate,
        notes: { note: note, date: date },
      };
      country.cities.unshift(newCity);
      await country.save();
      res.json(country);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

// @router POST Photo URL & note - api/cities/:country_id/:city_id/photos
router.post('/:country_id/:city_id/photos', auth, async (req, res) => {
  try {
    const country = await Country.findById(req.params.country_id);
    const cities = country.cities;
    const city = cities.filter((city) => city.id == req.params.city_id);
    const photos = city[0].photos;
    city[0].photos.unshift(req.body);

    await country.save();
    res.json(country);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

module.exports = router;
