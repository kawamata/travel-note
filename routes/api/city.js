const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const auth = require('../../middleware/auth');

const Country = require('../../models/Country');
const moment = require('moment');

// @router GET City - api/city/:country_id/:city_id
router.get('/:country_id/:city_id', auth, async (req, res) => {
  try {
    const country = await Country.findById(req.params.country_id);
    const city = country.cities.filter(
      (city) => city._id == req.params.city_id
    );
    if (!city) {
      return res.status(400).json({ msg: 'This city cannot be found' });
    }
    res.json(city);
  } catch (err) {
    console.error(err);
    return res.status(500).json({ msg: 'Server error' });
  }
});

// @router POST City Note - api/city/:country_id/:city_id/note
router.post(
  '/:country_id/:city_id/note',
  auth,
  [check('note', 'Note is required').not().isEmpty()],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      const date = moment().format('MM/DD/YYYY');
      const country = await Country.findById(req.params.country_id);
      const city = country.cities.filter(
        (city) => city._id == req.params.city_id
      );

      const note = {
        note: req.body.note,
        date: date,
      };
      city[0].notes.unshift(note);
      await country.save();
      res.json(city[0]);
    } catch (err) {
      console.error(err);
      return res.status(500).json({ msg: 'Server error' });
    }
  }
);

// @router DELETE City Photo - api/city/photo/:photo_id
router.delete('/photo/:photo_id', auth, async (req, res) => {
  try {
    const country = await Country.findById(req.query.country_id);
    const city = country.cities.filter((city) => city._id == req.query.city_id);
    const photo = city[0].photos.filter(
      (photo) => photo._id == req.params.photo_id
    );
    const deleteIndex = city[0].photos
      .map((photo) => photo._id)
      .indexOf(req.params.photo_id);
    city[0].photos.splice(deleteIndex, 1);
    await country.save();
    res.json(city[0]);
  } catch (err) {
    console.error(err);
    return res.status(500).json({ msg: 'Server error' });
  }
});

// @router DELETE City Note - api/city/note/:note_id
router.delete('/note/:note_id', auth, async (req, res) => {
  try {
    const country = await Country.findById(req.query.country_id);
    const city = country.cities.filter((city) => city._id == req.query.city_id);
    const note = city[0].notes.filter((note) => note._id == req.params.note_id);
    const deleteIndex = city[0].notes
      .map((note) => note._id)
      .indexOf(req.params.note_id);
    city[0].notes.splice(deleteIndex, 1);
    await country.save();
    res.json(city[0]);
  } catch (err) {
    console.error(err);
    return res.status(500).json({ msg: 'Server error' });
  }
});

// @router DELETE City - api/city
router.delete('/', auth, async (req, res) => {
  try {
    const country = await Country.findById(req.query.country_id);
    const deleteIndex = country.cities
      .map((city) => city._id)
      .indexOf(req.query.city_id);

    country.cities.splice(deleteIndex, 1);
    await country.save();
    res.json(country);
  } catch (err) {
    console.error(err);
    return res.status(500).json({ msg: 'Server error' });
  }
});

// @router EDIT City Note - api/city/note/:note_id
router.put(
  '/note/:note_id',
  auth,
  [check('note', 'Note is required').not().isEmpty()],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      const country = await Country.findById(req.body.country_id);
      const city = country.cities.filter(
        (city) => city._id == req.body.city_id
      );
      const note = city[0].notes.filter(
        (note) => note._id == req.params.note_id
      );
      note[0].note = req.body.note;
      await country.save();
      res.json(city[0]);
    } catch (err) {
      console.error(err);
      return res.status(500).json({ msg: 'Server error' });
    }
  }
);

module.exports = router;
