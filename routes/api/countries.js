const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const auth = require('../../middleware/auth');

const Country = require('../../models/Country');

// @router GET api/countries/my
router.get('/my', auth, async (req, res) => {
  try {
    const allcountries = await Country.find({ userId: req.user.id });
    const countries = allcountries.filter(
      (country) => country.userId == req.user.id
    );
    if (!countries) {
      return res.status(400).json({ msg: 'There is no country added' });
    }
    res.json(countries);
  } catch (err) {
    console.error(err);
    return res.status(500).json({ msg: 'Server error' });
  }
});

// @router POST api/countries
router.post(
  '/',
  [
    auth,
    [
      check('countryname', 'Country name is required').not().isEmpty(),
      check('area', 'Area is required').not().isEmpty(),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      //   const user = await User.findById(req.user.id).select('-password');
      const { countryname, area } = req.body;
      const newCountry = new Country({
        countryname: countryname,
        area: area,
        userId: req.user.id,
      });
      await newCountry.save();
      const countries = await Country.find();
      res.json(countries);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

module.exports = router;
