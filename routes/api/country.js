const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');

const Country = require('../../models/Country');

// @router GET api/country/:country_id
router.get('/:country_id', auth, async (req, res) => {
  try {
    const country = await Country.findById(req.params.country_id);
    if (!country) {
      return res.status(400).json({ msg: 'There is no this country added' });
    }
    res.json(country);
  } catch (err) {
    console.error(err);
    return res.status(500).json({ msg: 'Server error' });
  }
});

// @router DELETE api/country
router.delete('/', auth, async (req, res) => {
  try {
    await Country.findOneAndRemove({ _id: req.query.country_id });
    const countries = await Country.find();
    res.json(countries);
  } catch (err) {
    console.error(err);
    return res.status(500).json({ msg: 'Server error' });
  }
});

module.exports = router;
